//
//  City.swift
//  CallChallenge
//
//  Created by Martin on 2/18/20.
//  Copyright © 2020 Martin. All rights reserved.
//

import Foundation

struct CityWeather : Codable {
    let name : String
    let coord : Coord
    let main : Main
    
    struct Coord : Codable {
        let lat : Double    
        let lon : Double
    }
    
    struct Main : Codable {
        let pressure : Double
        let humidity : Double
        let temp : Double
        let temp_max : Double
        let temp_min : Double
    }
}


