//
//  AppDataAPI.swift
//  CallChallenge
//
//  Created by Martin on 2/18/20.
//  Copyright © 2020 Martin. All rights reserved.
//

import Foundation

protocol DataLayer {
    func saveItem(_ item: Any)
    func retrieveList() -> Array<Any>
    func saveList(_ list:[Any])
}

class DataLayerWeatherApp: DataLayer {
    
    // CONST
    let MAX_ITEMS_TO_SAVE = 5
    let NS_USER_DEFAULTS_DATA_NAME = CONFIG.NS_USER_DEFAULTS_NAME.rawValue
    
    // VARS
    var citiesList : [String]
    
    init() {
        citiesList = []
    }
    
    /*****************************************************************/
    // MARK: --------------- API Methods -----------------
    /*****************************************************************/
    
 
    func getWeather(_ city: String, completionHandler: @escaping (CityWeather) -> (), errorHandler: @escaping () -> ()) {
        
        // DATA FROM http://api.openweathermap.org/
        
        let session = URLSession.shared
        let urlString = "\(ENDPOINTS.BASE_URL.rawValue)\(ENDPOINTS.WEATHER_ENDPOINT.rawValue)\(city)\(ENDPOINTS.APP_ID.rawValue)"
        let myUrl = URL(string: urlString)!
                    
        let _ = session.dataTask(with: myUrl) { data, response, error in
            if error != nil {
                self.handleClientError(error!)
                return
            }
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    self.handleServerError(response!)
                    errorHandler()
                    return
            }
            guard let mime = response?.mimeType, mime == "application/json" else {
                print(ERROR_CODES.MIME_TYPE_ERROR.rawValue)
                return
            }
            
            if let cityWeather = self.createWeatherFromJSON(data!) {
                completionHandler(cityWeather)
            }
        }.resume()
    }
    
    func saveItem(_ item:Any) { // Saves to NSUserDefaults (could be CoreData or any other data storage, but this do the job)
        
        if let path = Bundle.main.path(forResource: "citiesList", ofType: "plist") {
                // TODO: TODO: save and read as plist
        }
        
        if let item = item as? String {
            var citiesList : [String] = []
            
            if UserDefaults.standard.object(forKey: NS_USER_DEFAULTS_DATA_NAME) != nil {
                citiesList = UserDefaults.standard.stringArray(forKey: NS_USER_DEFAULTS_DATA_NAME)!
                if (citiesList.count == MAX_ITEMS_TO_SAVE) {
                    citiesList.removeFirst()
                }
                citiesList.append(item)
            } else {
                citiesList = [item] // First save
            }
            
            UserDefaults.standard.setValue(citiesList, forKey: NS_USER_DEFAULTS_DATA_NAME)
        } else {
            print("\(ERROR_CODES.NOT_STRING_ERROR.rawValue)")
        }
    }
    
    func retrieveList() -> (Array<Any>) {
        if UserDefaults.standard.object(forKey: NS_USER_DEFAULTS_DATA_NAME) != nil {
            let cities = UserDefaults.standard.stringArray(forKey: NS_USER_DEFAULTS_DATA_NAME)!
            print("Reading from History \(cities)")
            return cities
        }
        return []
    }
    
    func saveList(_ items:[Any]) {
        print("Saving to history: \(items)")
        UserDefaults.standard.setValue(items, forKey: NS_USER_DEFAULTS_DATA_NAME)
    }
    
    func createWeatherFromJSON(_ data:Data) -> CityWeather? {
        let jsonDecoder = JSONDecoder()
        
        do {
            let cityData = try jsonDecoder.decode(CityWeather.self, from: data)
            return cityData
        } catch {
            print("Unexpected error: \(error).")
        }
        
        return nil
    }
    
    
    /*****************************************************************/
    // MARK: ----------------- Error Handling -----------------
    /*****************************************************************/
    
    
    func handleClientError(_ error: Error) {
        // TODO: SHOW ERROR MESSAGE TO USER
        print("\(ERROR_CODES.CLIENT_ERROR.rawValue) \(error)")
    }
    
    func handleServerError(_ error: Any) {
        // TODO: SHOW ERROR MESSAGE TO USER
        print("\(ERROR_CODES.SERVER_ERROR.rawValue) \(error)")
    }
}
