//
//  Codes.swift
//  CallChallenge
//
//  Created by Martin on 2/18/20.
//  Copyright © 2020 Martin. All rights reserved.
//

import Foundation

enum ERROR_CODES : String {
    case URL_ERROR = "Error: cannot create URL"
    case MIME_TYPE_ERROR = "Wrong MIME type!"
    case JSON_ERROR = "JSON error: "
    case SERVER_ERROR = "Oops! There was a problem with the server: "
    case CLIENT_ERROR = "There was an error: "
    case NOT_STRING_ERROR = "You are only allowed to save items of type 'City'"
}

enum ENDPOINTS : String {
    case BASE_URL = "https://api.openweathermap.org/"
    case WEATHER_ENDPOINT = "data/2.5/weather?q="
    case APP_ID = "&appid=f3716955e0e25b36bdbe360961d6a76d"
}

enum CONFIG : String {
    case DELETE_RECORD_NOTIFICATION = "DeleteCity" // For cell observer..
    case NS_USER_DEFAULTS_NAME = "CITIES_SAVED"
}

enum SEGUES_CODES : String {
    case HISTORY_SEGUE_ID = "TO_HISTORY"
}

enum LABELS : String { // TODO: Make localizable
    case MAX_TEMP_LBL = "Max. T:   "
    case MIN_TEMP_LBL = "Min.  T:   "
    case PRESSURE_LBL = "Pressure:   "
    case HUMIDITY_LBL = "Humidity:   "
    
    case SEARCH_LBL = "Search"
    case HISTORY_TITLE_LBL = "Search History"
    case HISTORYCELL_DELETE_LBL = "DELETE"
    
    case CITY_NOT_FOUND_LBL = "City not found"
    
    case HPA = "hPa"
    case CENTIGRADES = " °C"
}
