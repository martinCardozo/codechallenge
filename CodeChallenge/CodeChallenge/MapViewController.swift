//
//  ViewController.swift
//  IT Crowd Code Challenge
//
//  Created by Martin on 2/18/20.
//  Copyright © 2020 Martin. All rights reserved.
//

import UIKit
import MapKit

protocol NewCitySearch {
     func searchCity(_ cityName: String)
}

class MapViewController: UIViewController, NewCitySearch {

    // IB
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var citySearchName: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var weatherInfoView: UIView!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tempLbl: UILabel!
    @IBOutlet weak var maxTempLbl: UILabel!
    @IBOutlet weak var minTempLbl: UILabel!
    @IBOutlet weak var humidityLbl: UILabel!
    @IBOutlet weak var pressureLbl: UILabel!
    @IBOutlet weak var cityNotFoundLbl: UILabel!
    
    // UI VALUES
    let MAP_REGION_RADIUS : CLLocationDistance = 15000
    let FADE_OUT_DURATION = 0.4
    let FADE_IN_DURATION = 1.2
    let SHADOW_OPACITY = 0.8
    let CORNER_RADIUS = 7
    
    // TEMP CONSTANTS
    let KELVIN_CELCIUS_DIFF = 273.0
    let PRESSURE_UNIT = LABELS.HPA.rawValue
    let TEMPERATURE_UNIT = LABELS.CENTIGRADES.rawValue
    
    // MODULES
    let dataLayer = DataLayerWeatherApp()
    var history : SearchHistory
    
    var cityToLoad = ""
    let MAX_CITIES_TO_SAVE = 5
    
    /*****************************************************************/
    // MARK: ----------- View Controller Methods --------------
    /*****************************************************************/
    
    required init?(coder aDecoder: NSCoder) {
        history = SearchHistory(dataLayer)
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.UIInit()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /****************************************************************/
    // MARK: -------------- Logic Methods --------------
    /****************************************************************/
    
    
    @IBAction func searchCityButton(_ sender: UIButton) {
        if citySearchName.text != "" {
            searchCity(citySearchName.text!)
        }
    }
    
    func searchCity(_ cityName: String) {
        if cityName != "" {
            cityToLoad = cityName
            self.loadCity()
        }
    }
    
    func loadCity() {
        disableUI()
        
        // Asks the weather info to the API and, if found it, save to history and show to user
        // TODO: TODO: Use promises (using PromiseKit)
        
        /*firstly {
            //self.dataLayer.getWeather(self.cityToLoad)
            print("Step 1")
            return enableUI()
        }.then {
            /*self.history.append(self.cityToLoad)
            DispatchQueue.main.async {
                self.enableUI()
                self.setPanelInfo(cityWeather)
                self.fadeInWeatherInfo();
                self.setMapOnLocation(cityWeather.GPSPos.lat, cityWeather.GPSPos.long)
            }*/
            print("Step 2")
            return enableUI()
        }.then {
            print("Step 3")
            return enableUI()
        }.catch {_ in
            // any errors in the whole chain land here
        }*/
        
        self.dataLayer.getWeather(self.cityToLoad, completionHandler: { (_ cityWeather:CityWeather) -> () in
            self.history.append(self.cityToLoad)
            DispatchQueue.main.async {
                self.enableUI()
                self.setPanelInfo(cityWeather)
                self.fadeInWeatherInfo();
                self.setMapOnLocation(cityWeather.coord.lat, cityWeather.coord.lon)
            }
        }, errorHandler: {
            DispatchQueue.main.async {
                self.enableUI()
                self.showCityNotFound()
            }
        })
    }
    
    func setPanelInfo(_ cityWeather: CityWeather) {
        tempLbl.text = "\(Int(cityWeather.main.temp - KELVIN_CELCIUS_DIFF))\(TEMPERATURE_UNIT)"
        maxTempLbl.text = "\(LABELS.MAX_TEMP_LBL.rawValue)\(String(Int(cityWeather.main.temp_max - KELVIN_CELCIUS_DIFF)))\(TEMPERATURE_UNIT)"
        minTempLbl.text = "\(LABELS.MIN_TEMP_LBL.rawValue)\(String(Int(cityWeather.main.temp_min - KELVIN_CELCIUS_DIFF)))\(TEMPERATURE_UNIT)"
        pressureLbl.text = "\(LABELS.PRESSURE_LBL.rawValue)\(String(Int(cityWeather.main.pressure))) \(PRESSURE_UNIT)"
        humidityLbl.text = "\(LABELS.HUMIDITY_LBL.rawValue)\(String(Int(cityWeather.main.humidity))) %"
    }
    
    func setMapOnLocation(_ lat: Double, _ long: Double) {
        let GPSLocation = CLLocation(latitude: lat, longitude: long)
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(GPSLocation.coordinate, MAP_REGION_RADIUS, MAP_REGION_RADIUS)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUES_CODES.HISTORY_SEGUE_ID.rawValue {
            if let destinationVC = segue.destination as? HistoryViewController {
                destinationVC.history = history
                destinationVC.numberOfCitiesToSave = MAX_CITIES_TO_SAVE
                destinationVC.delegate = self
            }
        }
    }
    
    /****************************************************************/
    // MARK: -------------- UI Methods --------------
    /****************************************************************/
    
    
    func UIInit() {
        searchButton.layer.cornerRadius = CGFloat(CORNER_RADIUS);
        historyButton.layer.cornerRadius = CGFloat(CORNER_RADIUS);
        cityNotFoundLbl.layer.cornerRadius = CGFloat(CORNER_RADIUS);
        tempLbl.layer.cornerRadius = CGFloat(CORNER_RADIUS);
        weatherInfoView.layer.cornerRadius = 15;
        mapView.layer.cornerRadius = 10;
        
        tempLbl.layer.masksToBounds = true
        cityNotFoundLbl.layer.masksToBounds = true
        tempLbl.layer.zPosition = 100
        cityNotFoundLbl.layer.zPosition = 101
        cityNotFoundLbl.text = LABELS.CITY_NOT_FOUND_LBL.rawValue
        
        self.addShadow(view: searchButton)
        self.addShadow(view: weatherInfoView)
    }
    
    func disableUI() {
        searchButton.isUserInteractionEnabled = false
        historyButton.isUserInteractionEnabled = false
        citySearchName.isUserInteractionEnabled = false
        
        // VISUAL FEEDBACK
        citySearchName.backgroundColor = UIColor.darkGray;
        searchButton.alpha = 0.3
        fadeOutWeatherInfo()
    }
    
    func enableUI() {
        searchButton.isUserInteractionEnabled = true
        historyButton.isUserInteractionEnabled = true
        citySearchName.isUserInteractionEnabled = true
        
        // VISUAL FEEDBACK
        UIView.animate(withDuration: TimeInterval(FADE_IN_DURATION), animations: { // With fade in for more polished product
            self.citySearchName.backgroundColor = UIColor.white;
            self.searchButton.alpha = 1
        }, completion: { (value: Bool) in })
    }
    
    func fadeInWeatherInfo() {
        UIView.animate(withDuration: TimeInterval(FADE_IN_DURATION), animations: {
            self.tempLbl.alpha = 1
            self.maxTempLbl.alpha = 1
            self.minTempLbl.alpha = 1
            self.pressureLbl.alpha = 1
            self.humidityLbl.alpha = 1
        }, completion: { (value: Bool) in })
    }
    
    func fadeOutWeatherInfo() {
        UIView.animate(withDuration: TimeInterval(FADE_OUT_DURATION), animations: {
            self.tempLbl.alpha = 0
            self.maxTempLbl.alpha = 0
            self.minTempLbl.alpha = 0
            self.pressureLbl.alpha = 0
            self.humidityLbl.alpha = 0
        }, completion: { (value: Bool) in })
    }
    
    func showCityNotFound() {
        UIView.animate(withDuration: TimeInterval(FADE_OUT_DURATION), animations: {
            self.cityNotFoundLbl.alpha = 1
        }, completion: { (value: Bool) in
            UIView.animate(withDuration: TimeInterval(self.FADE_IN_DURATION), animations: {
                self.cityNotFoundLbl.alpha = 0
            }, completion: { (value: Bool) in })}
        )
    }

    func addShadow(view: UIView) {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = Float(SHADOW_OPACITY)
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 8
    }
}

