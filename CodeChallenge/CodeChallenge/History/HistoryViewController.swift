//
//  HistoryViewController.swift
//  CallChallenge
//
//  Created by Martin on 2/21/20.
//  Copyright © 2020 Martin. All rights reserved.
//

import Foundation
import UIKit

class HistoryViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {

    // UI
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    
    // VARS
    private var historyList : [String] = []
    private let dataLayer : DataLayerWeatherApp?
    var history : SearchHistory?
    var numberOfCitiesToSave = 0
    private var cityToSearch : String?
    
    var delegate : NewCitySearch?
    
    private let MY_CELL_IDENTIFIER = "historyCell"
    
    /*****************************************************************/
    // MARK: ----------- View Controller Methods --------------
    /*****************************************************************/
    
    required init?(coder aDecoder: NSCoder) {
        self.dataLayer = DataLayerWeatherApp()
        self.history = SearchHistory(self.dataLayer!)
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.UIInit()
        
        historyList = (history?.getHistory() as! [String]).reversed()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:HistoryCell = self.tableView.dequeueReusableCell(withIdentifier: MY_CELL_IDENTIFIER)! as! HistoryCell
        cell.row = indexPath.row
        cell.cityLbl?.text = self.historyList[indexPath.row]
        cell.deleteButton?.titleLabel?.text = LABELS.HISTORYCELL_DELETE_LBL.rawValue
        cell.deleteFunc = self.deleteRecord
        self.addShadow(view: cell.deleteButton)
        
        return cell
    }
    
    private func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cityToSearch = historyList[indexPath.item]
        self.history?.saveHistory(historyList.reversed())
        self.delegate?.searchCity(self.historyList[indexPath.item])
        self.navigationController?.popViewController(animated: true)
    }
    
    /****************************************************************/
    // MARK: ----------- Other Methods --------------  
    /****************************************************************/
    
    func deleteRecord(row: Int) {
        historyList.remove(at: row)
        let indexPath = IndexPath(row: row, section: 0)
        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        tableView.reloadData()
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /****************************************************************/
    // MARK: -------------- UI Methods --------------
    /****************************************************************/
    
    func UIInit(){
        titleLbl.text = LABELS.HISTORY_TITLE_LBL.rawValue
        backButton.layer.cornerRadius = CGFloat(7);
        
        var frame = self.tableView.frame;
        frame.size.height = self.tableView.contentSize.height;
        self.tableView.frame = frame;
        tableView.layer.cornerRadius = CGFloat(10)
        tableView.backgroundColor = UIColor.black
    }
    
    func addShadow(view: UIView) {
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 6
    }
}
