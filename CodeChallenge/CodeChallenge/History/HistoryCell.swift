//
//  HistoryCell.swift
//  CallChallenge
//
//  Created by Martin on 2/21/20.
//  Copyright © 2020 Martin. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {
    // IB
    @IBOutlet var cityLbl: UILabel!
    @IBOutlet var deleteButton: UIButton!
    
    var deleteFunc: ((Int)->Void)?
    
    var row = 0
    
    @IBAction func deleteItem(_ sender: UIButton) {
        guard (deleteFunc?(row)) != nil else { return }
    }
}


