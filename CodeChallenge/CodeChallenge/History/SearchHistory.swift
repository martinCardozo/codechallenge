//
//  SearchHistory.swift
//  CallChallenge
//
//  Created by Martin on 2/18/20.
//  Copyright © 2020 Martin. All rights reserved.
//

import Foundation

class SearchHistory {
    private var dataLayer : DataLayer
    
    init(_ dataLayer: DataLayer) {
        self.dataLayer = dataLayer
    }
    
    func append(_ item: Any) {
        dataLayer.saveItem(item);
    }

    func getHistory() -> Array<Any> {
        return dataLayer.retrieveList()
    }
    
    func saveHistory(_ list:[Any]) {
        dataLayer.saveList(list)
    }
}
